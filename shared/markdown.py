from markdownx.utils import markdownify as base_markdownify
from django import template
import re


def markdownify(md_content, context=None):
    """ Transform a markdown string to an HTML representation, handling non-breaking
    spaces """
    nbsped = add_nbsp(md_content)
    templatified = md_templatify(nbsped, context)
    return base_markdownify(templatified)


def md_templatify(content, context):
    """ Interpret the markdown as a Django template within a context

    NOTE: this is awfully inefficient, as we parse the template at **every** rendering.
    If we have enough affluence we might want to memcache this, or think of something
    more intelligent such as DB-caching. """

    if context is None:
        return content
    templ = template.Template(content)
    return templ.render(context=template.Context(context))


def add_nbsp(content):
    """ Replace regular spaces with non-breaking spaces within a text around relevant
    symbols """
    NBSP_BEFORE = [":", "!", "?", "»", ":", ";", "—", "€"]
    NBSP_AFTER = ["«", "—"]

    re_before = re.compile("(?: *\r?\n *| +)([{}])".format("".join(NBSP_BEFORE)))
    re_after = re.compile("([{}])(?: +| *\r?\n *)".format("".join(NBSP_AFTER)))

    content = re_before.sub(r"&nbsp;\1", content)
    content = re_after.sub(r"\1&nbsp;", content)
    return content
