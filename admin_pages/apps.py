from django.apps import AppConfig


class AdminPagesConfig(AppConfig):
    name = "admin_pages"
    verbose_name = "Pages d'administration"
