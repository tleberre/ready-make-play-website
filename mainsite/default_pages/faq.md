# FAQ

## Puis-je manger sur place ?

Oui. Nous fournissons sur le week-end un total de 5 repas, du vendredi soir au
dimanche midi (cuisinés sur place et à des prix très raisonnables, à indiquer
lors de votre inscription). Vous pourrez également manger sur place de la
nourriture extérieure si vous le souhaitez (mais ça vous reviendra sûrement
plus cher). La seule condition est que vous ne salissiez pas le matériel de jeu
! Tous nos repas seront végétariens, mais des options véganes seront possibles.
Vous pouvez dès maintenant consulter les [menus]({% url "mainsite:md_page" "meals" %}).

## Puis-je dormir sur place ? Jusqu’à quand puis-je rester sur les lieux de l’événement ?

Non, il sera interdit de dormir sur place et nous vous demanderons de trouver
vous-même comment vous loger. Des organisateur·ices·s seront présent·e·s 24h sur 24 pour
que vous puissiez continuer à game designer aussi tard que vous le souhaitez,
mais nous vous déconseillons fortement de faire des nuits blanches — il vous
sera important d’avoir les idées claires ! Vous pourrez en revanche entrer et
sortir de l'ENS à toute heure.

## Puis-je m'inscrire seul ?

Oui, il est possible de s'inscrire seul·e. Nous pensons qu'il est préférable de vivre cette expérience à plusieurs avec un groupe d'ami·e·s.
Nous souhaitons en conséquence que tout le monde participe par équipe, et si vous vous inscrivez seul·e, nous vous trouverons des partenaires !

## Je suis dans une équipe avec trop ou trop peu de membres est-ce grave ?

Non, nous conseillons toutes les équipes de comporter entre {{ site_config.undercrowded_threshold }} et {{ site_config.overcrowded_threshold }} participant·e·s mais si vous voulez dépasser ces limites, nous ne vous forcerons pas la main. Dans tous les cas, vous pourrez toujours séparer ou fusionner des équipes sur place si besoin.

## Je ne peux pas être présent tout le week-end, puis-je participer ?

Nous conseillons fortement à chaque équipe d'être présente sur toute la durée du week-end. Si vous savez que vous devrez interrompre la game jam pour faire autre chose, nous vous déconseillons fortement de participer, mais si le reste de l'équipe est d'accord, il n'y a bien sûr pas de problème. Vous pouvez également venir {{ site_config.event_time_betatest|date:'l \à h\h' }} pour la phase de test !

## Je ne peux pas être là à {{ site_config.event_time_start|date:'H\h \l\e l' }} soir, puis-je participer ?

Si vous arrivez avant la révélation du thème {{ site_config.event_time_theme_reveal|date:'\à H\h' }}, vous pouvez participer. Venir avant permet de se familiariser avec le poste de travail, le local dans lequel vous allez faire votre jeu, et le matériel à votre disposition. C'est aussi utile pour que nous puissions vous accueillir tranquillement avant le début. Si vous arrivez encore plus tard, référez-vous à la question précédente.

## Je n’ai jamais fait de game design. Est-ce pénalisant ?

Bien sûr que non ! La plupart des participant·e·s n’auront jamais designé de jeu
avant. Cet évènement est justement une occasion pour des passionné·e·s de
s’essayer à une expérience nouvelle, et n'est de toute façon pas un concours !

## Puis-je repartir avec mon jeu ?

Oui ! Le matériel fourni est offert et vous pourrez repartir avec votre
prototype. Nous veillerons tout de même à ce que vous n'en abusiez pas pour repartir avec une quantité anormale de matériel.

## Je n’ai pas fini mon jeu à temps, est-ce grave ?

Non. L’objectif est que {{ site_config.event_time_betatest|date:'l \à H\h' }}
vous ayez un prototype de jeu jouable afin que du public extérieur puisse venir
le tester. Mais nous vous laisserons faire de petits ajustements après, par exemple si
votre jeu a besoin d’équilibrage. Si vous n’avez pas de jeu jouable à temps,
c’est dommage, mais nous vous laisserons évidemment le terminer et repartir avec.
Et puis, une fois l'évènement terminé, rien ne vous empêche de continuer plus tard !

## Faut-il amener du matériel ?

Nous fournissons le principal matériel nécessaire à la création de votre jeu. En
revanche, pensez à emporter de quoi écrire, des feuilles de brouillon, et au
moins un ordinateur par équipe (cela facilitera par exemple la création de
cartes).

## Quel sera la matériel fourni ?

Nous aurons de tout ce qu'il faut pour faire un jeu : des pions génériques, de meeples, des dés, des figurines exotiques récupérées dans des vieux jeux... Vous trouverez de quoi faire des cubes de ressources, des pions personnages, des jetons, vous aurez des sabliers, divers objets avec lesquels expérimenter. Nous vous permettrons aussi d'imprimer des cartes et de faire des plateaux et des tuiles.

## Comment pourrons-nous fabriquer des cartes ?

Pour faire les cartes, nous imprimerons vos visuels sur des feuilles, et ce sera à vous de les découper (matériel fourni !) et des les insérer dans les protèges cartes que vous nous fournirons. Pour la création des cartes, c'est à vous de jouer ! Nous vous fournirons une façon pratique de faire des templates pour générer des cartes à la chaîne, simplement à partir d'un tableur. N'hésitez pas à illustrer vous même vos cartes si vous vous sentez de le faire !

## Et des plateaux ?

Vous pourrez imprimer les visuels de vos plateaux et nous mettrons du carton fin à votre disposition pour faire des plateaux rigides et plats. Cette méthode vous permettra aussi de faire des tuiles.

## Comment trouver la réponse à une question qui n'est pas dans cette FAQ ?

Vous pouvez [nous contacter par mail]({% url "mainsite:md_page" "who_are_we" %}#contact_us), et nous nous ferons une joie d'y répondre, et de la rajouter à cette FAQ !
