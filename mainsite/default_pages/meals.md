# Menus

Nous vous proposons des repas rapides et pas chers à manger directement sur le lieu de l'événement. La plupart des repas sont végétariens (Le barbecue a une option végétarienne, mais contient de la viande pour celleux qui le souhaitent).


*Note : les menus peuvent être amenés à changer d'ici à l'événement.*

## Vendredi soir

* Sandwhich au humous ou au chèvre

## Samedi midi

* Barbecue : légumes et saucisses grillées

## Samedi soir

* Taboulé

## Dimanche midi : brunch

* Viennoiseries
* Reste des repas précédents
* Jus de fruits et boissons chaudes
