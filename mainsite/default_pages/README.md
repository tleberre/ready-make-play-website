# Default pages

This directory contains the source markdown files used as default pages for the
Markdown pages.

This directory is only here **for convenience**, and is not used in any way by
the website. You should import the page you're working on in your local site's
DB, then re-generate the fixture using:

```bash
./manage.py dumpdata mainsite.MarkdownPage --indent 2 > mainsite/fixtures/base_pages.json
```
