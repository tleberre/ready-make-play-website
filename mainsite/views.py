from django.views.generic import DetailView, TemplateView
from django.contrib.sitemaps import Sitemap
from django.utils.safestring import mark_safe
from shared.markdown import markdownify
from .models import MarkdownPage
from site_wide_settings.models import SiteWideSettings


class MarkdownTemplateView(DetailView):
    model = MarkdownPage
    template_name = "mainsite/base_markdown.html"

    def get_markdown_body(self):
        return mark_safe(
            markdownify(self.object.content, context=self.get_markdown_context_data())
        )

    def get_markdown_context_data(self, **kwargs):
        context = kwargs
        context["site_config"] = SiteWideSettings.get_solo()
        return context

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["markdown_body"] = self.get_markdown_body()
        return context


class RobotsView(TemplateView):
    template_name = "robots.txt"
    content_type = "text/plain"


class MarkdownPagesSitemap(Sitemap):
    def items(self):
        return MarkdownPage.objects.all()

    def lastmod(self, obj):
        return obj.lastmod

    def priority(self, obj):
        # Priorize home page over the rest in search results
        if not obj.slug:
            return 0.8
        else:
            return None # defaults to 0.5 when unset
