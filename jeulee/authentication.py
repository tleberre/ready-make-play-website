from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend


class AuthByEmailBackend(ModelBackend):
    """ Authentication backend to authentify via email + password and ignore username
    altogether """

    def authenticate(self, request, username=None, password=None, **kwargs):
        UserModel = get_user_model()
        norm_email = username.strip().lower()
        try:
            user = UserModel.objects.get(email=norm_email)
        except UserModel.DoesNotExist:
            return None
        else:
            if user.check_password(password):
                return user
        return None
