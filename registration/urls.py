from django.urls import path, include

from . import views

app_name = "registration"

urlpatterns_user = [
    path("login/", views.LoginView.as_view(), name="login"),
    path("logout/", views.LogoutView.as_view(), name="logout"),
    path("password_reset/", views.PasswordResetView.as_view(), name="password_reset"),
    path(
        "password_reset_done/",
        views.PasswordResetDoneView.as_view(),
        name="password_reset_done",
    ),
    path(
        "password_reset_confirm/<str:uidb64>/<str:token>/",
        views.PasswordResetConfirmView.as_view(),
        name="password_reset_confirm",
    ),
    path(
        "password_reset_complete",
        views.PasswordResetCompleteView.as_view(),
        name="password_reset_complete",
    ),
]

urlpatterns = [
    path("", views.RegistrationView.as_view(), name="registration"),
    path("team/", views.TeamView.as_view(), name="team"),
    path("auth/", include(urlpatterns_user)),
    path("register_start/", views.RegisterStartView.as_view(), name="register_start"),
    path(
        "register_join_team_needs_link/",
        views.RegisterWithTeamNeedsLinkView.as_view(),
        name="register_join_team_needs_link",
    ),
    path(
        "join_team/<slug:invite_token>/", views.JoinTeamView.as_view(), name="join_team"
    ),
    path("register_infos/", views.RegisterInfosView.as_view(), name="register_infos"),
    path(
        "email_confirmation_needed/",
        views.EmailConfirmationNeededView.as_view(),
        name="email_confirmation_needed",
    ),
    path(
        "confirm_email/<uidb64>/<token>/",
        views.ConfirmEmailView.as_view(),
        name="confirm_email",
    ),
    path(
        "register_meals/", views.MealInfosSelectionView.as_view(), name="register_meals"
    ),
    path(
        "update_personal_infos/",
        views.UpdatePersonalInfosView.as_view(),
        name="update_personal_infos",
    ),
    path(
        "change_password/", views.PasswordChangeView.as_view(), name="password_change"
    ),
]
