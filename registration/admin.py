from django.contrib import admin
from .models import MealInfos, Contestant, Team

admin.site.register(MealInfos)
admin.site.register(Contestant)
admin.site.register(Team)
