""" Forms for the registration/infos update processes """

from django import forms
from django.contrib.auth import password_validation, authenticate
from django.contrib.auth.models import User as DjangoUser
from django.utils.safestring import mark_safe
from django.urls import reverse

from shared.forms import FormRenderMixin
from .models import Contestant, MealInfos

from site_wide_settings.models import SiteWideSettings

from django.utils.translation import gettext_lazy as _

from django.contrib.auth import forms as auth_forms

from datetime import date


def password_criterions_html():
    def wrap_str(s, tagopen, tagclose=None):
        if not tagclose:
            tagclose = tagopen
        return "<{}>{}</{}>".format(tagopen, s, tagclose)

    criterions = password_validation.password_validators_help_texts()
    criterions_html = wrap_str(
        "\n".join(map(lambda crit: wrap_str(crit, "li"), criterions)),
        'ul class="helptext"',
        "ul",
    )
    return mark_safe(criterions_html)


class AuthenticationForm(FormRenderMixin, auth_forms.AuthenticationForm):
    username = forms.EmailField(label="Adresse mail")
    password = forms.CharField(
        widget=forms.PasswordInput,
        label="Mot de passe",
    )


class PasswordResetForm(FormRenderMixin, auth_forms.PasswordResetForm):
    email = forms.EmailField(label="Adresse mail")


class SetPasswordForm(FormRenderMixin, auth_forms.SetPasswordForm):
    tooltip_helptexts = ["new_password1"]

    new_password1 = forms.CharField(
        label=_("New password"),
        widget=forms.PasswordInput(attrs={"autocomplete": "new-password"}),
        strip=False,
        help_text=password_criterions_html(),
    )


class RegistrationPersonalInfosForm(FormRenderMixin, forms.Form):
    """Personal informations we gather about a contestant at registration time"""

    name = forms.CharField(
        max_length=512,
        label="Nom",
        help_text="Tel que vous l'écrivez habituellement, avec prénom",
    )
    email = forms.EmailField(label="Adresse mail")
    in_psl = forms.BooleanField(
        label="Je viens d'un établissement membre ou partenaire de l'Université PSL",
        help_text="L'inscription est ouverte à tou·te·s que vous soyez étudiant·e "
        "à PSL ou non",
        required=False,
    )
    school = forms.ChoiceField(
        label="Établissement PSL",
        required=False,
        choices=[("", "-- Sélectionnez votre école --")] + Contestant.PSL_SCHOOLS,
    )
    password = forms.CharField(
        widget=forms.PasswordInput,
        help_text=password_criterions_html(),
        label="Mot de passe",
    )
    password_confirm = forms.CharField(
        widget=forms.PasswordInput,
        label="Mot de passe (confirmation)",
    )

    code_of_conduct = forms.BooleanField(
        label=(
            """J'ai lu et j'accepte la <a href="{}">charte de bonne conduite</a> de """
            """Ready, Make, Play!"""
        ).format(reverse("md_page", args=["code_of_conduct"], urlconf="mainsite.urls"))
    )

    tooltip_helptexts = ["password"]
    field_groups = [
        ["name", "email"],
        ["in_psl", "school"],
        ["password", "password_confirm"],
        ["code_of_conduct"],
    ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.label_suffix = ""

    @staticmethod
    def normalize_email(email):
        """Returns a normalized email"""
        return email.lower()

    def clean_email(self):
        """Check email uniqueness"""
        email = self.cleaned_data["email"]
        norm_email = self.normalize_email(email)
        if DjangoUser.objects.filter(email=norm_email).count() > 0:
            raise forms.ValidationError(
                "Un compte avec cette adresse mail existe déjà. Peut-être êtes-vous "
                "déjà inscrit·e ?"
            )
        return norm_email

    def clean_school(self):
        """Validate `school`: if `in_psl` is checked, this field is required."""
        in_psl = self.cleaned_data["in_psl"]
        school = self.cleaned_data["school"]
        if in_psl and not school:
            raise forms.ValidationError("Veuillez renseigner votre établissement PSL.")
        if not in_psl:
            return None
        return school

    def clean_password(self):
        """Check password strength"""
        password = self.cleaned_data["password"]
        password_validation.validate_password(password)
        return password

    def clean_password_confirm(self):
        """Check that both passwords match"""
        cleaned_data = super().clean()
        password = cleaned_data.get("password")
        password_confirm = cleaned_data.get("password_confirm")
        if not password:
            return None
        if password != password_confirm:
            raise forms.ValidationError(
                "Les deux mots de passe ne sont pas identiques."
            )
        return cleaned_data

    def create_contestant(self):
        """Creates a User and Contestant objects.
        The form is expected to be already validated upon calling this method"""

        user = DjangoUser.objects.create_user(
            username=self.cleaned_data["email"],
            email=self.cleaned_data["email"],
            password=self.cleaned_data["password"],
        )
        user.last_name = self.cleaned_data["name"]
        user.save()
        contestant = Contestant.objects.create(
            user=user,
            team=None,
            school=self.cleaned_data["school"],
        )

        return contestant


class PersonalInfosForm(FormRenderMixin, forms.ModelForm):
    """Form to update one's personal informations after account creation"""

    class Meta:
        model = DjangoUser
        fields = ["email", "last_name"]
        labels = {
            "email": "Adresse mail",
            "last_name": "Nom",
        }
        help_texts = {
            "email": "Si vous la changez, il faudra confirmer la nouvelle adresse",
            "last_name": "Tel que vous l'écrivez habituellement, avec prénom",
        }

    @staticmethod
    def normalize_email(email):
        """Returns a normalized email"""
        return email.lower()

    def clean_email(self):
        """Check email uniqueness"""
        email = self.cleaned_data["email"]
        if email == self.instance.email:
            return email
        norm_email = self.normalize_email(email)
        if DjangoUser.objects.filter(email=norm_email).count() > 0:
            raise forms.ValidationError(
                "Un autre compte avec cette adresse mail existe déjà."
            )
        return norm_email

    def save(self, *args, commit=True, **kwargs):
        email = self.cleaned_data["email"]
        email_changed = email != self.instance.username
        user = super().save(*args, commit=False, **kwargs)
        user.username = email
        if email_changed:
            contestant = user.contestant
            contestant.email_confirmed = False
            contestant.save()
        if commit:
            user.save()
        return user


def hot_meal_price():
    """Return the formatted price of a hot meal"""
    config = SiteWideSettings.get_solo()
    return "{} €".format(config.meal_price)


def breakfast_price():
    """Return the formatted price of a breakfast"""
    config = SiteWideSettings.get_solo()
    return "{} €".format(config.breakfast_price)


def brunch_price():
    """Return the formatted price of a brunch"""
    config = SiteWideSettings.get_solo()
    return "{} €".format(config.brunch_price)


def bbq_price():
    """Return the formatted price of a bbq"""
    config = SiteWideSettings.get_solo()
    return "{} €".format(config.bbq_price)


class MealsForm(FormRenderMixin, forms.ModelForm):
    """Form to select which meals a contestant will take"""

    class Meta:
        model = MealInfos
        fields = "__all__"
        exclude = ["contestant"]

    field_groups = [
        list(MealInfos.MEAL_NAMES.keys()),
        ["meal_restrictions", "other_meal_restrictions"],
        ["accepted_price"],
    ]
    tooltip_helptexts = ["meal_restrictions"]

    def __init__(self, *args, contestant, **kwargs):
        self.contestant = contestant
        super().__init__(*args, **kwargs)
        for meal in MealInfos.HOT_MEALS:
            self.fields[meal].help_text = hot_meal_price()
        for meal in MealInfos.BREAKFASTS:
            self.fields[meal].help_text = breakfast_price()
        for meal in MealInfos.BRUNCHS:
            self.fields[meal].help_text = brunch_price()
        for meal in MealInfos.BBQ_MEALS:
            self.fields[meal].help_text = bbq_price()
        meals_freeze_date = SiteWideSettings.get_solo().meals_freeze_date
        if date.today() >= meals_freeze_date:
            for field in self.fields.values():
                field.disabled = True

    def clean_other_meal_restrictions(self):
        """Validate `other_meal_restrictions`: if `meal_restrictions` is `other`, this
        field is required."""
        meal_restrictions = self.cleaned_data["meal_restrictions"]
        other_meal_restrictions = self.cleaned_data["other_meal_restrictions"]
        if meal_restrictions == "other" and not other_meal_restrictions:
            # Required if `meal_restrictions == other`
            raise forms.ValidationError(
                "Veuillez renseigner vos restrictions alimentaires spéciales."
            )
        if meal_restrictions != "other":
            return None
        return other_meal_restrictions

    def clean(self):
        """Check that the contestant agreed to pay their meals and is not changing them after the freeze date"""
        cleaned_data = super().clean()

        meals_freeze_date = SiteWideSettings.get_solo().meals_freeze_date
        if date.today() >= meals_freeze_date and self.has_changed():
            cleaned_data = self.initial
            raise forms.ValidationError(
                "La date limite pour les changements d'options de repas est dépassée"
            )

        has_meals = False
        for meal in MealInfos.HOT_MEALS + MealInfos.BREAKFASTS + MealInfos.BRUNCHS:
            wants_meal = cleaned_data[meal]
            if wants_meal:
                has_meals = True
                break

        accepted_price = cleaned_data["accepted_price"]
        if has_meals and not accepted_price:
            raise forms.ValidationError(
                "Vous devez vous engager à payer vos repas si vous en réservez au "
                "moins un."
            )
        return cleaned_data

    def save(self, *args, commit=True, **kwargs):
        meals = super().save(*args, commit=False, **kwargs)
        meals.contestant = self.contestant
        if commit:
            meals.save()
        return meals


class UpdatePasswordForm(FormRenderMixin, forms.Form):
    """Form to update one's password"""

    current_password = forms.CharField(
        widget=forms.PasswordInput,
        label="Mot de passe actuel",
    )
    password = forms.CharField(
        widget=forms.PasswordInput,
        help_text=password_criterions_html(),
        label="Nouveau mot de passe",
    )
    password_confirm = forms.CharField(
        widget=forms.PasswordInput,
        label="Nouveau mot de passe (confirmation)",
    )

    tooltip_helptexts = ["password"]

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop("user", None)
        super().__init__(*args, **kwargs)

    def clean_current_password(self):
        """Check current password correctness"""
        cur_password = self.cleaned_data["current_password"]
        if authenticate(username=self.user.email, password=cur_password) != self.user:
            raise forms.ValidationError("Votre mot de passe actuel est incorrect.")
        return cur_password

    def clean_password(self):
        """Check password strength"""
        password = self.cleaned_data["password"]
        password_validation.validate_password(password)
        return password

    def clean_password_confirm(self):
        """Check that both passwords match"""
        cleaned_data = super().clean()
        password = cleaned_data.get("password")
        password_confirm = cleaned_data.get("password_confirm")
        if not password:
            return None
        if password != password_confirm:
            raise forms.ValidationError(
                "Les deux mots de passe ne sont pas identiques."
            )
        return cleaned_data

    def apply(self):
        """Apply the password change, assuming validation was already passed"""
        self.user.set_password(self.cleaned_data["password"])
        self.user.save()
