from django.db import models
from django.contrib.auth.models import User as DjangoUser

from django.utils.translation import gettext as _

from site_wide_settings.models import SiteWideSettings

import random
import string
from collections import OrderedDict


def generate_invite_token():
    """ Generates a random invite token """
    slug_char_pool = string.ascii_letters + string.digits
    return "".join([random.choice(slug_char_pool) for _ in range(10)])


class Team(models.Model):
    """ A team of contestants """

    invite_token = models.SlugField(
        max_length=10, unique=True, default=generate_invite_token
    )

    class Meta:
        verbose_name = "équipe"

    def garbage_collect(self):
        """ self-destroy if there are no remaining members """
        if self.members.count() == 0:
            self.delete()

    def has_invalid_members(self):
        return self.members.filter(email_confirmed=False).exists()

    def __str__(self):
        return "{} membres, {}".format(self.members.count(), self.invite_token)

    def warnings(self):
        """ Returns a list of warnings in case of incomplete team subscription """
        config = SiteWideSettings.get_solo()
        out = []
        member_count = self.members.all().count()
        if self.has_invalid_members():
            out.append("invalid_members")
        if member_count < config.undercrowded_threshold:
            out.append("undercrowded")
        elif member_count > config.overcrowded_threshold:
            out.append("overcrowded")
        return out


class Contestant(models.Model):
    """ A constestant """

    class Meta:
        verbose_name = "participant·e"
        verbose_name_plural = "participant·e·s"
        ordering = ["user__last_name"]

    user = models.OneToOneField(DjangoUser, on_delete=models.CASCADE, primary_key=True)
    email_confirmed = models.BooleanField(default=False)
    team = models.ForeignKey(
        Team,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        help_text="If null, the contestant doesn't belong to a team yet",
        related_name="members",
    )

    PSL_SCHOOLS = [
        ("chimie", "Chimie ParisTech"),
        ("cnsad", "Conservatoire National Supérieur d’Art Dramatique"),
        ("chartes", "École nationale des chartes"),
        ("ens", "École normale supérieure"),
        ("ephe", "École Pratique des Hautes Études"),
        ("espci", "ESPCI Paris"),
        ("mines", "MINES ParisTech"),
        ("observatoire", "Observatoire de Paris"),
        ("dauphine", "Université Paris-Dauphine"),
        ("college-de-france", "Collège de France"),
        ("curie", "Institut Curie"),
        ("cnrs", "CNRS"),
        ("inria", "Inria"),
        ("inserm", "Inserm"),
        ("cnsmdp", "Conservatoire national supérieur de musique et de danse de Paris"),
        ("efeo", "École française d’Extrême-Orient"),
        ("ensad", "École nationale supérieure des Arts Décoratifs"),
        ("femis", "La Fémis"),
        ("beaux-arts", "Les Beaux-Arts de Paris"),
        ("h4", "Lycée Henri-IV"),
    ]

    school = models.CharField(
        max_length=512, verbose_name="école", blank=True, null=True, choices=PSL_SCHOOLS
    )

    def __str__(self):
        return "{} <{}>".format(self.user.last_name, self.user.username)

    def warnings(self):
        """ Returns a list of warnings in case of incomplete subscription """
        out = []
        if not self.email_confirmed:
            out.append("email")
        if not hasattr(self, "meals"):
            out.append("meal")
        return out


class MealInfos(models.Model):
    """ A contestant's meal choices """

    class Meta:
        verbose_name = "choix de repas"
        verbose_name_plural = "choix de repas"

    contestant = models.OneToOneField(
        Contestant, on_delete=models.CASCADE, primary_key=True, related_name="meals"
    )

    HOT_MEALS = [
        "meal_fri_dinner",
        "meal_sat_dinner",
    ]
    BBQ_MEALS = [
        "meal_sat_lunch",
    ]

    BREAKFASTS = ["meal_sat_breakfast"]

    BRUNCHS = ["meal_sun_brunch"]

    MEAL_NAMES = OrderedDict(
        [
            ("meal_fri_dinner", _("Vendredi soir")),
            ("meal_sat_breakfast", _("Samedi petit-déjeuner")),
            ("meal_sat_lunch", _("Samedi midi")),
            ("meal_sat_dinner", _("Samedi soir")),
            ("meal_sun_brunch", _("Dimanche brunch")),
        ]
    )

    MEAL_RESTRICTIONS = [
        ("", _("Aucun régime particulier")),
        ("vege", _("Végétarien")),
        ("vegan", _("Vegan")),
        ("other", _("Autre régime, intolérance, allergie, …")),
    ]

    meal_fri_dinner = models.BooleanField(MEAL_NAMES["meal_fri_dinner"])
    meal_sat_breakfast = models.BooleanField(MEAL_NAMES["meal_sat_breakfast"])
    meal_sat_lunch = models.BooleanField(MEAL_NAMES["meal_sat_lunch"])
    meal_sat_dinner = models.BooleanField(MEAL_NAMES["meal_sat_dinner"])
    meal_sun_brunch = models.BooleanField(MEAL_NAMES["meal_sun_brunch"])

    meal_restrictions = models.CharField(
        _("Restrictions alimentaires"),
        max_length=10,
        help_text=(
            "Nous tâcherons de satisfaire ce régime. "
            "En cas d'impossibilité, si vous réservez des repas, nous vous "
            "préviendrons en avance."
        ),
        default="",
        blank=True,
        choices=MEAL_RESTRICTIONS,
    )

    # To be filled if `meal_restrictions` is `other`
    other_meal_restrictions = models.CharField(
        _("Précisez lequel"),
        blank=True,  # unless `meal_restrictions == other`, see validation
        null=True,
        help_text=_(
            "Précisez également s'il s'agit d'une intolérance, allergie, … "
            "et sa sévérité"
        ),
        max_length=256,
    )

    accepted_price = models.BooleanField(
        _("Je m'engage à payer mes repas"),
        help_text=_("Même si je ne les mange finalement pas&nbsp;!"),
        default=False,
    )

    def hot_meals_count(self):
        """ Return the total number of hot meals taken in the selection """
        return sum(map(lambda meal: int(getattr(self, meal)), self.HOT_MEALS))

    def breakfast_count(self):
        """ Return the total number of breakfasts taken in the selection """
        return sum(map(lambda meal: int(getattr(self, meal)), self.BREAKFASTS))

    def brunch_count(self):
        """ Return the total number of brunchs taken in the selection """
        return sum(map(lambda meal: int(getattr(self, meal)), self.BRUNCHS))

    def bbq_count(self):
        """ Return the total number of brunchs taken in the selection """
        return sum(map(lambda meal: int(getattr(self, meal)), self.BBQ_MEALS))

    def number(self):
        """ Return the total number of meals taken in the selection """
        return self.hot_meals_count() + self.breakfast_count() + self.brunch_count() + self.bbq_count()

    def price(self):
        """ Return the price corresponding to the meals taken """
        config = SiteWideSettings.get_solo()
        return (
            self.breakfast_count() * config.breakfast_price
            + self.hot_meals_count() * config.meal_price
            + self.brunch_count() * config.brunch_price
            + self.bbq_count() * config.bbq_price
        )

    def __str__(self):
        return "Repas de {} [{}{}{}{}{}] {}".format(
            self.contestant.user.last_name,
            "v" if self.meal_fri_dinner else "-",
            "s" if self.meal_sat_breakfast else "-",
            "s" if self.meal_sat_lunch else "-",
            "s" if self.meal_sat_dinner else "-",
            "d" if self.meal_sun_brunch else "-",
            self.meal_restrictions
            if self.meal_restrictions != "other"
            else self.other_meal_restrictions,
        )
