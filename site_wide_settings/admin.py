from django.contrib import admin
from solo.admin import SingletonModelAdmin
from .models import SiteWideSettings


admin.site.register(SiteWideSettings, SingletonModelAdmin)
