# This makefile wraps around django commands
# for faster integrations

# See 'make help' for a list of useful targets

# ==================================================
# Constants
# ===================================================

PYTHON := python3
MANAGER := manage.py
PIP := $(PYTHON) -m pip
DB := db.sqlite3

PRECOMMIT = pre-commit
MYPY = mypy

SETTINGS = jeulee/settings.py
SETTINGS_SRC = $(PWD)/jeulee/settings.dev.py

# set to ON/OFF to toggle ANSI escape sequences
COLOR = ON

# Uncomment to show commands
# VERBOSE = TRUE

# padding for help on targets
# should be > than the longest target
HELP_PADDING = 15

# ==================================================
# Make code and variable setting
# ==================================================

ifeq ($(COLOR),ON)
	color_yellow = \033[93;1m
	color_orange = \033[33m
	color_red    = \033[31m
	color_green  = \033[32m
	color_blue   = \033[34;1m
	color_reset  = \033[38;22m
endif

define print
	@echo "$(color_yellow)$(1)$(color_reset)"
endef

# =================================================
# Default target
# =================================================

default: serve

# =================================================
# Specific rules
# =================================================

$(SETTINGS):
	$(call print,Creating $@)
	ln -s "$(SETTINGS_SRC)" $@

# =================================================
# Special Targets
# =================================================

# No display of executed commands.
$(VERBOSE).SILENT:

.PHONY: default help serve migrate clean adduser shell

help: ## Show this help
	@echo "$(color_yellow)make:$(color_reset) list of useful targets :"
	@egrep -h '\s##\s' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "  $(color_blue)%-$(HELP_PADDING)s$(color_reset) %s\n", $$1, $$2}'

serve: $(SETTINGS) ## Run the django server
	$(call print,Running server, accessible from http://localhost:8000)
	$(PYTHON) $(MANAGER) runserver

migrate: $(SETTINGS) ## Make and run migrations
	$(call print,Migrating database)
	$(PYTHON) $(MANAGER) makemigrations
	$(PYTHON) $(MANAGER) migrate

clean: ## Remove migrations and delete database
	$(call print,Removing migrations and database)
	find . -path "*/migrations/*.py" -not -name "__init__.py" -not -path "*/venv/*" -delete
	find . -path "*/migrations/*.pyc" -not -path "*/venv/*" -delete
	rm $(DB)

adduser: $(SETTINGS) ## Create a new superuser
	$(call print,Creating a new superuser)
	$(PYTHON) $(MANAGER) createsuperuser

shell: $(SETTINGS) ## Run django's shell
	$(call print,Starting django's shell)
	$(PYTHON) $(MANAGER) shell

# =================================================
# Tests and checks
# =================================================

.PHONY: static test preprod mypy

static: $(SETTINGS) ## collect static files
	$(call print,Collecting static files)
	$(PYTHON) $(MANAGER) collectstatic

test: $(SETTINGS) ## Tests all the apps with django's tests
	$(call print,Running django tests)
	$(PYTHON) -Wa $(MANAGER) test

mypy: $(SETTINGS) ## Typecheck all file
	$(call print,Running mypy)
	$(MYPY) . --exclude /migrations/

preprod: mypy test static ## Prepare and check production
	$(PYTHON) $(MANAGER) check --deploy

# =================================================
# Installation
# =================================================

.PHONY: install install-devel setup setup-devel

install: $(SETTINGS)
	$(call print,Installing dependencies)
	$(PIP) install --upgrade pip
	$(PIP) install -r requirements.txt

install-devel: $(SETTINGS)
	$(call print,Installing development dependencies)
	$(PIP) install --upgrade pip
	$(PIP) install -r requirements-devel.txt
	$(call print,Setting up pre-commit)
	$(PRECOMMIT) install

setup: $(SETTINGS) install migrate ## Install dependencies and make migrations
setup-devel: $(SETTINGS) install-devel migrate ## Install development dependencies and make migrations
