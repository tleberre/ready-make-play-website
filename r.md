# Serveur photos

Ce répo contient le code source du serveur photo.

## Installation rapide

1. Il est conseillé d'utiliser un [environement virtuel](https://docs.python.org/3/tutorial/venv.html), installable au choix depuis `apt` ou `pip` :

	```
	sudo apt install python3-venv
	pip3 install venv
	```

2. Installer toutes les dépendances:

	```
	git clone TODO URL &&
	cd serveurphotos &&
	python3 -m venv venv &&
	source venv/bin/activate &&
	make setup
	```

	Pour une installation de dévelopement, utiliser `make setup-devel` pour avoir les dépendances additionnelles

3. Lancer le serveur:

	```
	make serve
	```

	Le site devrait être accessible à [http://localhost:8000](http://localhost:8000). Vous pouvez interrompre le serveur avec `Ctrl+C` depuis le terminal.
